<?php get_header(); ?>

  <main class="site-main subpage" role="main">
    <div class="padding-wrapper padding-full">

      <div class="text-wrapper">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <h1><?php the_title(); ?></h1>

          <div class="entry-content">

            <?php if( get_post_type() == 'post' ): ?>

              <p class="published-date">Published <?php the_date(); ?></p>

            <?php endif; ?>


            <?php the_content(); ?>

            <?php if( get_the_category() ): ?>

              <p>Categories: <?php the_category(' | '); ?></p>

            <?php endif; ?>

            <?php if( get_tags() ): ?>

              <p><?php the_tags('Tags: ', ' | '); ?></p>

            <?php endif; ?>

          </div>

        <?php endwhile; endif; ?>

        <?php if( get_post_type() == 'post' ): ?>

          <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="button button-primary">< Back to all posts</a>

        <?php endif; ?>

        <?php // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || get_comments_number() ) : ?>

          <div class="comments-section">

            <?php comments_template(); ?>

          </div>

        <?php endif; ?>

        <div class="comments-navigation">

          <?php the_post_navigation( array(
            'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'hawkwood' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'hawkwood' ) . '</span> <span class="nav-title">%title</span>',
            'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'hawkwood' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'hawkwood' ) . '</span> <span class="nav-title">%title</span>',
          ) ); ?>

        </div>

      </div>

    </div>
  </main>

<?php get_footer(); ?>

<?php if( function_exists( 'get_field' ) ): ?>

  <?php
    $banner = wp_get_attachment_image_src( get_field( 'page_banner' ), 'banner' );
    $defaultBanner = wp_get_attachment_image_src( get_field( 'default_banner_image', 'options' ), 'banner' );
  ?>

  <?php if( get_field( 'page_banner' ) ): ?>

    <?php if( is_front_page() ): ?>

      <div class="page-banner homepage-banner" style="background-image: url(<?php echo $banner[0]; ?>);">

        <div class="page-banner-content">

          <div class="padding-wrapper">

            <h2><?php the_field( 'banner_content' ); ?></h2>

            <?php if( get_field( 'banner_link' ) ): ?>

              <a href="<?php the_field( 'banner_link' ); ?>" class="button button-accent"><?php the_field( 'banner_button_text' ); ?></a>

            <?php endif; ?>
            
          </div>

        </div>

      </div>

    <?php else: ?>

      <div class="page-banner subpage-banner" style="background-image: url(<?php echo $banner[0]; ?>);"></div>

    <?php endif; ?>

  <?php elseif( get_field( 'default_banner_image', 'options' ) ): ?>

    <?php if( is_front_page() ): ?>

      <div class="page-banner homepage-banner" style="background-image: url(<?php echo $defaultBanner[0]; ?>">

        <div class="page-banner-content">

          <div class="padding-wrapper flex-wrapper">

            <div>

              <?php if( get_field( 'banner_heading' ) ): ?>

                <h2 class="page-banner-heading"><?php the_field( 'banner_heading' ); ?></h2>

              <?php endif; ?>

              <?php if( get_field( 'banner_subheading' ) ): ?>

                <p class="page-banner-subheading"><?php the_field( 'banner_subheading' ); ?></p>

              <?php endif; ?>

              <?php if( get_field( 'banner_button_link' ) ): ?>

                <a href="<?php the_field( 'banner_button_link' ); ?>" class="button button-accent"><?php the_field( 'banner_button_text' ); ?></a>

              <?php endif; ?>

            </div>

          </div>

        </div>
      </div>

    <?php else: ?>

      <div class="page-banner subpage-banner" style="background-image: url(<?php echo $defaultBanner[0]; ?>"></div>

    <?php endif; ?>

  <?php endif; ?>

<?php endif; ?>
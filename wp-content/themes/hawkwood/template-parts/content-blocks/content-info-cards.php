<div class="content-block text-center">
  <div class="padding-wrapper">
    <div class="medium-wrapper">

      <div class="text-wrapper">

        <h2 class="section-title section-title-center"><?php the_sub_field( 'section_title' ); ?></h2>

        <?php the_sub_field( 'section_copy' ); ?>

      </div>

      <?php if( have_rows( 'info_cards' ) ): ?>

        <div class="info-cards">

          <?php while( have_rows( 'info_cards' ) ): the_row(); ?>

            <?php if( get_sub_field( 'image_card') ): ?>

              <?php $background = wp_get_attachment_image_src( get_sub_field( 'image' ), 'hawkwood-medium' ); ?>

              <div class="info-card image-card" style="background-image: url(<?php echo $background[0]; ?>);"></div>

            <?php else: ?>

              <div class="info-card text-card">
                
                <h3><?php the_sub_field( 'card_heading' ); ?></h3>

                <p><?php the_sub_field( 'card_text' ); ?></p>

              </div>

            <?php endif; ?>

          <?php endwhile; ?>

        </div>

      <?php endif; ?>

    </div>
  </div>
</div>
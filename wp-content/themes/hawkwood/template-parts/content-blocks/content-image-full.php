<?php $background = wp_get_attachment_image_src( get_sub_field( 'image' ), 'full-page' ); ?>

<div class="content-block full-width image-full" style="background-image: url(<?php echo $background[0]; ?>);">
</div>
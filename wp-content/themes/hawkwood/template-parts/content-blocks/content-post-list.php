<div class="content-block">
  <div class="padding-wrapper">
    
    <div class="text-wrapper text-center">

      <?php if( get_sub_field( 'section_title' ) ): ?>

        <h2 class="section-title section-title-center"><?php the_sub_field( 'section_title' ); ?></h2>

      <?php endif; ?>

      <div>

        <?php the_sub_field( 'text_block' ); ?>

      </div>

    </div>

    <?php if( get_sub_field( 'show_featured' ) === true ) {

      $args = array(
        'post_type' => 'post',
        'orderby' => 'date',
        'order' => 'ASC',
        'meta_key' => 'featured',
        'meta_value' => true
      );

      $query = new WP_Query( $args );

    } else {

      $args = array(
        'post_type' => 'post',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => '3',
      );

      $query = new WP_Query( $args );

    }

    ?>

    <?php if( $query->have_posts() ): ?>

      <div class="post-list small-wrapper">

        <?php while( $query->have_posts() ): $query->the_post(); ?>

          <div class="image-left image-left-full">

            <?php if( has_post_thumbnail($post->ID) ): ?>

              <?php $background = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hawkwood-medium' ); ?>

              <div class="half-block image-fill" style="background-image: url(<?php echo $background[0]; ?>);"></div>

            <?php endif; ?>

            <div class="half-block block-padding">

              <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <div class="content-wrapper text-wrapper">

                  <div class="entry-header">

                    <h3><?php the_title(); ?></h3>

                  </div>
                
                  <div class="entry-content">

                    <div class="entry-excerpt">
                      <p>
                        <?php 
                          $content = get_the_excerpt();
                          echo wp_trim_words( $content , '20' ); 
                        ?>
                      </p>
                    </div>

                    <a class="button button-accent" href="<?php the_permalink(); ?>">Read More</a>

                  </div>

                </div>

              </article>

            </div>

          </div>

        <?php endwhile; ?>

      </div>

      <div class="text-center">
        <a class="button button-primary" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">See All Posts</a>
      </div>

    <?php endif; ?>

  </div>
</div>
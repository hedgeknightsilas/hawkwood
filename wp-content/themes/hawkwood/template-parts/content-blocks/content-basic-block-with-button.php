<div class="content-block">
  <div class="padding-wrapper">
    <div class="text-wrapper">

      <?php if( get_sub_field( 'section_title' ) ): ?>

        <h2 class="section-title"><?php the_sub_field( 'section_title' ); ?></h2>

      <?php endif; ?>

      <div>

        <?php the_sub_field( 'content' ); ?>

      </div>

      <?php if( get_sub_field( 'page_link' ) ): ?>

        <a href="<?php the_sub_field( 'page_link' ); ?>" class="button button-primary">
          <?php if( get_sub_field( 'button_text' ) ): ?>

            <?php the_sub_field( 'button_text' ); ?>

          <?php endif; ?>

        </a>

      <?php endif; ?>

    </div>
  </div>
</div>
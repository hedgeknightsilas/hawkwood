<div class="content-block">
  <div class="padding-wrapper">
    <div class="text-wrapper">

      <?php if( get_sub_field( 'section_title' ) ): ?>

        <h2 class="section-title"><?php the_sub_field( 'section_title' ); ?></h2>

      <?php endif; ?>

      <?php the_sub_field( 'content' ); ?>

    </div>
  </div>
</div>
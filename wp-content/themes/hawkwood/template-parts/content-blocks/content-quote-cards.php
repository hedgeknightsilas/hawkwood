<div class="content-block">
  <div class="padding-wrapper">
    
    <div class="text-wrapper text-center">

      <?php if( get_sub_field( 'section_title' ) ): ?>

        <h2 class="section-title section-title-center"><?php the_sub_field( 'section_title' ); ?></h2>

      <?php endif; ?>

      <div>

        <?php the_sub_field( 'text_block' ); ?>

      </div>

    </div>

    <?php if( have_rows( 'quote_cards' ) ): ?>

      <div class="small-wrapper">

        <div class="quote-cards clearfix">

          <?php while( have_rows( 'quote_cards' ) ): the_row(); ?>

            <div class="quote-card">

              <div class="quote-wrapper">

                <i class="fa fa-quote-left fa-2x"></i>

                <div class="quote">

                  <p><?php the_sub_field( 'quote' ); ?></p>

                  <p>— <?php the_sub_field( 'quote_author' ); ?></p>

                </div>

              </div>

            </div>

          <?php endwhile; ?>

        </div>

      </div>

    <?php endif; ?>

  </div>
</div>
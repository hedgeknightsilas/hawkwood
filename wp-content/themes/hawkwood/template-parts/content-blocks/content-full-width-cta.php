<div class="content-block full-width">

  <div class="full-width-cta light-background block-padding">
    <div class="padding-wrapper">
      <div class="text-wrapper">

        <?php the_sub_field( 'text_block' ); ?>

        <?php if( get_sub_field( 'page_link' ) ): ?>

          <a class="button button-accent" href="<?php the_sub_field( 'page_link' ); ?>">
            <?php the_sub_field( 'button_text' ); ?>    
          </a>

        <?php endif; ?>

      </div>
    </div>
  </div>

</div>
<div class="content-block full-width image-left image-left-full">

  <?php $background = wp_get_attachment_image_src( get_sub_field( 'image' ), 'hawkwood-medium' ); ?>

  <div class="half-block image-fill" style="background-image: url(<?php echo $background[0]; ?>);"></div>

  <div class="half-block block-padding light-background">

    <div class="content-wrapper">
    
      <div>

        <?php if( get_sub_field( 'section_title' ) ): ?>

          <h2 class="section-title"><?php the_sub_field( 'section_title' ); ?></h2>

        <?php endif; ?>

        <?php the_sub_field( 'copy' ); ?>

      </div>

      <?php if( get_sub_field( 'page_link' ) ): ?>

        <a class="button button-primary" href="<?php the_sub_field( 'page_link' ); ?>"><?php the_sub_field( 'button_text' ); ?></a>

      <?php endif; ?>

    </div>

  </div>

</div>
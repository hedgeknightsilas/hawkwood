<?php get_header(); ?>

<main class="site-main subpage" role="main">

  <header class="page-header">

    <div class="padding-wrapper">
      <div class="text-wrapper">

        <?php
          global $post;
          $category = get_the_category($post->ID);
          $categoryId = $category[0]->cat_ID;
          $queriedObject = get_queried_object();
        ?>

        <div class="main-wordpress-content">

          <h1>Currently Browsing: <?php single_cat_title(); ?></h1>

          <p><?php echo category_description($categoryId); ?></p>

        </div>

      </div>
    </div>

  </header>

  <div class="padding-wrapper">
    <div class="large-wrapper posts-sidebar-wrapper">

      <div class="posts-column">
        <div class="text-wrapper">

          <?php 
            $args = array(
              'post_type' => 'post',
              'category__in' => array($categoryId)
            );

            $the_query = new WP_Query( $args );

          ?>

          <?php if( $the_query->have_posts() ): ?>

            <div class="post-items">

              <?php while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                      <div class="entry-header">

                        <p class="byline"><?php the_time('F jS, Y') ?></p>

                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                        <div class="entry-meta">

                          <?php if( get_the_category() ): ?>

                            <p class="post-category"><?php the_category(' , '); ?></p>

                          <?php endif; ?>

                          <p class="comment-count"><?php comments_number( '0 Comments', '1 Comment', '%1$s Comments' ); ?></p>

                        </div>

                      </div>

                      <?php if( get_the_post_thumbnail() ): ?>

                        <div class="post-thumbnail">

                          <?php the_post_thumbnail( 'hawkwood-medium' ); ?>

                        </div>

                      <?php endif; ?>
                      
                      <div class="entry-content">

                        <div class="entry-excerpt"><?php the_excerpt(); ?></div>
                        <a class="button button-primary" href="<?php the_permalink(); ?>">Read More</a>

                      </div>

                    </article>

              <?php endwhile; ?>

            </div>

            <?php if( function_exists( 'wp_pagenavi') ): ?>

              <div class="navigation">

                <?php wp_pagenavi(); ?>

              </div>

            <?php else: ?>

              <?php the_posts_pagination( array(
                'prev_text'          => __( 'Previous page', 'hawkwood' ),
                'next_text'          => __( 'Next page', 'hawkwood' ),
                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'hawkwood' ) . ' </span>',
              ) ); ?>

            <?php endif; ?>

          <?php else : ?>

            <p><?php _e( 'Sorry, no posts matched your criteria.', 'hawkwood' ); ?></p> 

          <?php endif; ?>

        </div>
      </div>

      <?php if( is_active_sidebar( 'blog-sidebar' ) ): ?>

        <div class="sidebar">

          <?php dynamic_sidebar( 'blog-sidebar' ); ?>

        </div>

      <?php endif; ?>

    </div>
  </div>
</main>

<?php get_footer(); ?>


<?php

function hawkwood_customize_register( $wp_customize ) {
  $wp_customize->add_setting( 'accent_color' , array(
      'default'   => '#c9b04b',
      'transport' => 'postMessage',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'accent_color', array(
      'label'        => 'Accent Color',
      'section'    => 'hawkwood_colors',
      'settings'   => 'accent_color',
  ) ) );
  $wp_customize->add_section( 'hawkwood_colors' , array(
    'title'      => __( 'Theme Options', 'hawkwood' ),
    'priority'   => 30,
  ) );
}
add_action( 'customize_register', 'hawkwood_customize_register' );
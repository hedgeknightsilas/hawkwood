<?php get_header(); ?>

  <main class="site-main" role="main">
    <div class="padding-wrapper padding-full">

      <div class="text-wrapper">

        <div>
          <h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'hawkwood' ); ?></h1>
        </div>

        <div class="entry-content">
          <p>It looks like nothing was found at this location. Maybe try going back to the homepage and find the page from there? Or try searching for it.</p>

          <?php get_search_form(); ?>
        </div>

      </div>

    </div>
  </main>

<?php get_footer(); ?>

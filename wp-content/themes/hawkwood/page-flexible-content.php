<?php /* Template Name: Flexible Content */ ; ?>

<?php get_header(); ?>

  <main class="site-main" role="main">

    <?php if( have_posts() ): ?>

      <?php while( have_posts() ): the_post(); ?>

        <header class="page-header">
          <div class="padding-wrapper">
            <div class="text-wrapper">

              <?php if( !is_front_page() ): ?>

                <h1><?php the_title(); ?></h1>

              <?php endif; ?>

              <?php the_content(); ?>

            </div>
          </div>
        </header>

      <?php endwhile; ?>

    <?php endif; ?>

    <?php if( function_exists( 'get_field' ) ): ?>

      <?php if( have_rows( 'content_blocks' ) ): ?>

        <div class="flexible-content">

          <?php while( have_rows( 'content_blocks' ) ): the_row(); ?>

            <?php if( get_row_layout() == 'basic_content_block' ): ?>

              <?php get_template_part( 'template-parts/content-blocks/content', 'basic-content-block' ); ?>

            <?php elseif( get_row_layout() == 'basic_block_with_button' ): ?>

              <?php get_template_part( 'template-parts/content-blocks/content', 'basic-block-with-button' ); ?>

            <?php elseif( get_row_layout() == 'image_left' ): ?>

              <?php get_template_part( 'template-parts/content-blocks/content', 'image-left' ); ?>

            <?php elseif( get_row_layout() == 'image_left_full' ): ?>

              <?php get_template_part( 'template-parts/content-blocks/content', 'image-left-full' ); ?>

            <?php elseif( get_row_layout() == 'info_cards' ): ?>

              <?php get_template_part( 'template-parts/content-blocks/content', 'info-cards' ); ?>

            <?php elseif( get_row_layout() == 'full_width_cta' ): ?>

              <?php get_template_part( 'template-parts/content-blocks/content', 'full-width-cta') ; ?>

            <?php elseif( get_row_layout() == 'quote_cards' ): ?>

              <?php get_template_part( 'template-parts/content-blocks/content', 'quote-cards') ; ?>

            <?php elseif( get_row_layout() == 'post_list' ): ?>

              <?php get_template_part( 'template-parts/content-blocks/content', 'post-list') ; ?>

            <?php elseif( get_row_layout() == 'image_full' ): ?>

              <?php get_template_part( 'template-parts/content-blocks/content', 'image-full') ; ?>

            <?php endif; ?>

          <?php endwhile; ?>

        </div>

      <?php endif; ?>

    <?php endif; ?>

  </main>

<?php get_footer(); ?>

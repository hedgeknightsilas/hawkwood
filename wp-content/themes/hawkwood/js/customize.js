(function($) {
  wp.customize('accent_color', function(value) {
    value.bind(function(newval) {
      document.querySelector('body').style.setProperty('--accent-color', newval);
    });
  });
})(jQuery);

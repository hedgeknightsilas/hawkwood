<?php get_header(); ?>

<main class="site-main subpage" role="main">

  <header class="page-header">
    <div class="padding-wrapper">
      <div class="text-wrapper">

        <h1><?php single_post_title(); ?></h1>

        <?php 

          $posts_page_id = get_option( 'page_for_posts' ); 
          $posts_page = get_post( $posts_page_id );

        ?>

        <?php if( $posts_page->post_content ): ?>

          <p><?php echo $posts_page->post_content; ?></p>

        <?php endif; ?>

      </div>
    </div>
  </header>

  <div class="padding-wrapper">

    <div class="large-wrapper posts-sidebar-wrapper">

      <div class="posts-column">

        <div class="text-wrapper">

          <?php if ( have_posts() ) : ?>

            <div class="post-items">

              <?php while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                  <div class="entry-header">

                    <p class="byline"><?php the_time('F jS, Y') ?></p>

                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                    <div class="entry-meta">

                      <?php if( get_the_category() ): ?>

                        <p class="post-category"><?php the_category(' , '); ?></p>

                      <?php endif; ?>

                      <p class="comment-count"><?php comments_number( '0 Comments', '1 Comment', '%1$s Comments' ); ?></p>

                    </div>

                  </div>

                  <?php if( get_the_post_thumbnail() ): ?>

                    <div class="post-thumbnail">

                      <?php the_post_thumbnail( 'hawkwood-medium' ); ?>

                    </div>

                  <?php endif; ?>
                  
                  <div class="entry-content">

                    <div class="entry-excerpt"><?php the_excerpt(); ?></div>
                    <a class="button button-primary" href="<?php the_permalink(); ?>">Read More</a>

                  </div>

                </article>

              <?php endwhile; ?>

            </div>

          <?php else : ?>

            <p><?php _e( 'Sorry, no posts matched your criteria.', 'hawkwood' ); ?></p>

          <?php endif; wp_reset_query(); ?>

          <div class="page-links">

            <?php if( function_exists( 'wp_pagenavi') ): ?>

              <div class="navigation">

                <?php wp_pagenavi(); ?>

              </div>

            <?php else: ?>

              <?php the_posts_pagination( array(
                'prev_text'          => __( 'Previous page', 'hawkwood' ),
                'next_text'          => __( 'Next page', 'hawkwood' ),
                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'hawkwood' ) . ' </span>',
              ) ); ?>

            <?php endif; ?>

          </div>

        </div>

      </div>

      <?php if( is_active_sidebar( 'blog-sidebar' ) ): ?>

        <div class="sidebar">

          <?php dynamic_sidebar( 'blog-sidebar' ); ?>

        </div>

      <?php endif; ?>

    </div>

  </div>

</main>

<?php get_footer(); ?>


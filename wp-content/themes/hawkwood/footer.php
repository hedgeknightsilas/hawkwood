  </div><!-- #content -->

  <?php if( function_exists( 'get_field' ) ): ?>

    <?php if( have_rows( 'social_media', 'options' ) ): ?>

      <div class="footer-social">
        <div class="padding-wrapper">

          <div class="social-icons">

            <?php while( have_rows( 'social_media', 'options' ) ): the_row(); ?>

              <a href="<?php the_sub_field( 'social_link', 'options' ); ?>" target="_blank" rel="noopener">

                <?php the_sub_field( 'social_icon', 'options' ); ?>

              </a>

            <?php endwhile; ?>

          </div>
        
        </div>
      </div>

    <?php endif; ?>

  <?php endif; ?>

  <footer>

    <section class="footer-info">

        <?php $currentDate = date("Y"); ?>
        
        <?php if ($currentDate == "2018") {
          $date = $currentDate;
        } else {
          $date = "2018-" . $currentDate;
        }; ?>

        <p class="legal">&copy; <?php echo $date; ?> <?php bloginfo( 'name' ); ?></p>
        <p class="credits">Site design by: <a href="http://hedgeknightcreative.com">Hedge Knight Creative</a></p>

    </section>

    <a href="#page-top" class="scroll-button">
      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/arrow-up.svg" />
    </a>

  </footer>

<?php wp_footer(); ?>

</body>
</html>
